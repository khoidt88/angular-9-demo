import { Component } from '@angular/core';
import { CourseService } from './course.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [CourseService],
})
export class AppComponent {
  title = 'my-app';
  name = "Hoang Duyen";

  coursesRandom: string[] = [];
  coursesFull: string[] = [];

  constructor(private _courseService: CourseService) {
  }

  ngOnInit() {
    this.coursesFull = this._courseService.getCourses();
    this.coursesRandom = this._courseService.getRandomCourses(3);
  }
  randomCourse() {
    this.coursesRandom = this._courseService.getRandomCourses(3);
  }
  addCourse(name: string) {
    this._courseService.addCourse(name);
  }

  onClick() {
    console.log("onClick");
  }

  buttonDisabled = true;
  numberA: number = 10;
  numberB: number = 20;
  names = ['Hoang Duyen', 'Hong Nhung', 'Thuy', 'Ha', 'Loi'];
  subjects = ['Java', 'PHP', 'Node js', 'Angular', 'React Js'];

  birthday = new Date(1988, 3, 15);

}
